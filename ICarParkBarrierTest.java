import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ICarParkBarrierTest {

	private CarPark carPark;
	@Before
	public void setUp() throws Exception {
		carPark = CarPark.getInstance();
		carPark.CarIsParked(new Car());
		carPark.CarIsParked(new Car());
		carPark.CarIsParked(new Car());
	}

	@Test
	public void testIsFreeParkingSpot() {
		assertEquals(true, carPark.isFreeParkingSpot());
	}

}
