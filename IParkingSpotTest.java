import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IParkingSpotTest {

	private ParkingSpot parkingSpot;
	private CarPark carPark;
	@Before
	public void setUp() throws Exception {
		carPark = CarPark.getInstance();
		parkingSpot = new ParkingSpot();
		parkingSpot.setColumn(0);
		parkingSpot.setFloor(0);
		parkingSpot.setRow(0);
	}

	@Test
	public void testGetFreeParkingSpot() {
		assertEquals(parkingSpot, carPark.getFreeParkingSpot());
	}

}
