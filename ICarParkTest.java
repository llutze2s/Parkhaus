import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ICarParkTest {
	private CarPark carPark;

	@Before
	public void setUp() throws Exception {
		carPark = CarPark.getInstance();
		carPark.CarIsParked(new Car());
		carPark.CarIsParked(new Car());
		carPark.CarIsParked(new Car());
	}

	@Test
	public void testGetNumberOfCars() {
		assertEquals(3, carPark.getNumberOfCars());
	}


}
