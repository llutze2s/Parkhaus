import java.util.ArrayList;

public class CarPark implements IParkingSpot, ICarPark, ICarParkBarrier {
	public static int numberOfMaxCars = 500;
	private int numberOfCurCars = 0;
	private static CarPark me;
	private ArrayList<ParkingSpot> parkedCars;

	private CarPark() {
		me = new CarPark();
		initParkingSpots();
	}

	public static CarPark getInstance() {
		if (me != null)
			return me;
		else
			return new CarPark();
	}

	public ArrayList<ParkingSpot> getParkedCars() {
		return parkedCars;
	}

	public void setParkedCars(ArrayList<ParkingSpot> parkedCars) {
		this.parkedCars = parkedCars;
	}

	@Override
	public ParkingSpot getFreeParkingSpot() {
		for(ParkingSpot p : parkedCars) {
			if(p.getCar() != null)
				return p;
		}
		return null;
	}

	@Override
	public int getNumberOfCars() {
		return numberOfCurCars;
	}

	@Override
	public void CarIsParked(Car car) {
		ParkingSpot pSpot;
		if ((pSpot = getFreeParkingSpot()) != null) {
			pSpot.setCar(car);
			numberOfCurCars++;
		}
	}

	@Override
	public boolean isFreeParkingSpot() {
		return numberOfMaxCars >= numberOfCurCars;
	}

	@Override
	public void initParkingSpots() {
		int i = 0;
		while (i < 500) {
			ParkingSpot newParkingSpot = new ParkingSpot();
			newParkingSpot.setCar(null);
			if (getParkedCars().get(0) == null) {
				newParkingSpot.setRow(0);
				newParkingSpot.setFloor(0);
				newParkingSpot.setColumn(0);
				getParkedCars().add(newParkingSpot);
			} else {
				ParkingSpot spotBefore = getParkedCars().get(numberOfCurCars);
				if (spotBefore.getColumn() >= 20 && spotBefore.getRow() <= 20) {
					newParkingSpot.setRow(spotBefore.getRow() + 1);
					newParkingSpot.setColumn(0);
					newParkingSpot.setFloor(spotBefore.getFloor());
				} else if (spotBefore.getColumn() >= 20 && spotBefore.getRow() >= 20) {
					newParkingSpot.setRow(0);
					newParkingSpot.setColumn(0);
					newParkingSpot.setFloor(spotBefore.getFloor() + 1);
				} else {
					newParkingSpot.setRow(spotBefore.getRow());
					newParkingSpot.setColumn(spotBefore.getColumn());
					newParkingSpot.setFloor(spotBefore.getFloor());
				}
			}
			parkedCars.add(newParkingSpot);
			i++;
		}
	}
}
